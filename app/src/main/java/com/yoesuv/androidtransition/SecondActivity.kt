package com.yoesuv.androidtransition

import android.os.Build
import android.os.Bundle
import androidx.core.content.ContextCompat
import androidx.appcompat.app.AppCompatActivity
import android.transition.TransitionInflater
import android.view.MenuItem
import android.view.Window
import com.yoesuv.androidtransition.databinding.ActivitySecondBinding

class SecondActivity: AppCompatActivity() {

    companion object {
        const val TYPE_ANIMATION = "type_animation"
    }

    private var type: Int = 0
    private lateinit var binding:ActivitySecondBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        type = intent.getIntExtra(TYPE_ANIMATION, 0)
        explode()
        when (type) {
            1 -> overridePendingTransition(R.anim.slide_in_top, R.anim.stay)
            2 -> overridePendingTransition(R.anim.slide_in_bottom, R.anim.stay)
            3 -> overridePendingTransition(R.anim.slide_in_left, R.anim.stay)
            4 -> overridePendingTransition(R.anim.slide_in_right, R.anim.stay)
            5 -> overridePendingTransition(R.anim.slide_in_top, R.anim.slide_out_bottom)
            6 -> overridePendingTransition(R.anim.slide_in_bottom, R.anim.slide_out_top)
            7 -> overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right)
            8 -> overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left)
            9 -> overridePendingTransition(R.anim.slide_in_right, R.anim.scale_close)
        }
        binding = ActivitySecondBinding.inflate(layoutInflater)
        setContentView(binding.root)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        binding.imageViewSecondActivity.setImageDrawable(ContextCompat.getDrawable(this, R.drawable.van_gogh_starynight))
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if(item.itemId==android.R.id.home){
            onBackPressed()
        }
        return super.onOptionsItemSelected(item)
    }

    override fun onBackPressed() {
        super.onBackPressed()
        when (type) {
            1 -> overridePendingTransition(R.anim.stay, R.anim.slide_out_top)
            2 -> overridePendingTransition(R.anim.stay, R.anim.slide_out_bottom)
            3 -> overridePendingTransition(R.anim.stay, R.anim.slide_out_left)
            4 -> overridePendingTransition(R.anim.stay, R.anim.slide_out_right)
            5 -> overridePendingTransition(R.anim.slide_in_bottom, R.anim.slide_out_top)
            6 -> overridePendingTransition(R.anim.slide_in_top, R.anim.slide_out_bottom)
            7 -> overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left)
            8 -> overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right)
            9 -> overridePendingTransition(R.anim.scale_open, R.anim.slide_out_right)
        }
    }

    private fun explode(){
        if (type==10) {
            if (Build.VERSION.SDK_INT>= Build.VERSION_CODES.LOLLIPOP) {
                window.requestFeature(Window.FEATURE_CONTENT_TRANSITIONS)
                val explode = TransitionInflater.from(this).inflateTransition(R.transition.explode)
                window.enterTransition = explode
            }
        }
    }

}