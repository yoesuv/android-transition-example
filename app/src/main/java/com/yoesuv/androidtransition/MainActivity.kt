package com.yoesuv.androidtransition

import android.app.ActivityOptions
import android.content.Intent
import android.os.Build
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.yoesuv.androidtransition.databinding.ActivityMainBinding

class MainActivity : AppCompatActivity() {

    private lateinit var binding:ActivityMainBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        binding.buttonTop.setOnClickListener {
            val intent = Intent(this, SecondActivity::class.java)
            intent.putExtra(SecondActivity.TYPE_ANIMATION, 1)
            startActivity(intent)
        }

        binding.buttonBottom.setOnClickListener {
            val intent = Intent(this, SecondActivity::class.java)
            intent.putExtra(SecondActivity.TYPE_ANIMATION, 2)
            startActivity(intent)
        }

        binding.buttonLeft.setOnClickListener {
            val intent = Intent(this, SecondActivity::class.java)
            intent.putExtra(SecondActivity.TYPE_ANIMATION, 3)
            startActivity(intent)
        }

        binding.buttonRight.setOnClickListener {
            val intent = Intent(this, SecondActivity::class.java)
            intent.putExtra(SecondActivity.TYPE_ANIMATION, 4)
            startActivity(intent)
        }

        binding.buttonTop2.setOnClickListener {
            val intent = Intent(this, SecondActivity::class.java)
            intent.putExtra(SecondActivity.TYPE_ANIMATION, 5)
            startActivity(intent)
        }

        binding.buttonBottomTwo.setOnClickListener {
            val intent = Intent(this, SecondActivity::class.java)
            intent.putExtra(SecondActivity.TYPE_ANIMATION, 6)
            startActivity(intent)
        }

        binding.buttonLeftTwo.setOnClickListener {
            val intent = Intent(this, SecondActivity::class.java)
            intent.putExtra(SecondActivity.TYPE_ANIMATION, 7)
            startActivity(intent)
        }

        binding.buttonRightTwo.setOnClickListener {
            val intent = Intent(this, SecondActivity::class.java)
            intent.putExtra(SecondActivity.TYPE_ANIMATION, 8)
            startActivity(intent)
        }

        binding.buttonScale.setOnClickListener {
            val intent = Intent(this, SecondActivity::class.java)
            intent.putExtra(SecondActivity.TYPE_ANIMATION, 9)
            startActivity(intent)
        }

        binding.buttonTransitionExplode.setOnClickListener {
            val intent = Intent(this, SecondActivity::class.java)
            intent.putExtra(SecondActivity.TYPE_ANIMATION, 10)
            if(Build.VERSION.SDK_INT>=Build.VERSION_CODES.LOLLIPOP){
                val options = ActivityOptions.makeSceneTransitionAnimation(this)
                startActivity(intent, options.toBundle())
            } else {
                startActivity(intent)
            }
        }
    }
}
