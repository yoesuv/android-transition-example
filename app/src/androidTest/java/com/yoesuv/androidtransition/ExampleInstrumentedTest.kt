package com.yoesuv.androidtransition

import android.support.test.InstrumentationRegistry
import android.support.test.runner.AndroidJUnit4

import org.junit.Test
import org.junit.runner.RunWith

import org.junit.Assert.*

/**
 * Instrumented slide_in_bottom, which will execute on an Android device.
 *
 * See [testing documentation](http://d.android.com/tools/testing).
 */
@RunWith(AndroidJUnit4::class)
class ExampleInstrumentedTest {
    @Test
    fun useAppContext() {
        // Context of the app under slide_in_bottom.
        val appContext = InstrumentationRegistry.getTargetContext()
        assertEquals("com.yoesuv.androidtransition", appContext.packageName)
    }
}
