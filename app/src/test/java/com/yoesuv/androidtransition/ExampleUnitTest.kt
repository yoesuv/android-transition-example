package com.yoesuv.androidtransition

import org.junit.Test

import org.junit.Assert.*

/**
 * Example local unit slide_in_bottom, which will execute on the development machine (host).
 *
 * See [testing documentation](http://d.android.com/tools/testing).
 */
class ExampleUnitTest {
    @Test
    fun addition_isCorrect() {
        assertEquals(4, 2 + 2)
    }
}
